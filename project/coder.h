
/*
 * coder.h
 *
 *  Created for: GlobalLogic Basecamp
 *       Author: vitalii.lysenko
 *
 * Coder class header.
 *
 * You have to change this file to fix build errors, and make
 * the unit tests passed.
 *
 */
#include <string>
#include <stdexcept>
#include <limits>
#include <cstring>
#include <cstdlib>
#include <iostream>


#ifndef CODER_H
#define CODER_H

/*
 * This function was pre-compiled and is provided as a part of the
 * static library.
 *
 */

extern void encode( char*, int );

//#pragma pack(push, 1)

/*
 * Coder class header.
 *
 */
class Coder 
{
public:
	Coder() : m_buf(0), m_size(0) {}
	Coder(const Coder& smth) 
	{  
		m_size = smth.m_size;
        m_buf = (char *)malloc(m_size);
        strcpy(m_buf, smth.m_buf);
	}

	void set( const char* buffer, int size_of_buf );
	void Convert(unsigned int val);
	Coder& operator=( Coder &something )
	{ 
        m_size = something.m_size;
        m_buf = (char *)malloc(m_size);
        strcpy(m_buf, something.m_buf);
        return *this;
	}
	
	//getters
	const char* buf(){ return m_buf; };
	int size(){ return m_size; };
	//coder-decoder
	void encode();
	void decode();
private:
	char* m_buf;
	unsigned int m_size;
};
//#pragma pack(pop)

#endif // CODER_H