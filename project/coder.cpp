/*
 * coder.cpp
 *
 *  Created for: GlobalLogic Basecamp
 *       Author: vitalii.lysenko
 *
 * Coder class source.
 *
 * You may have to change this file to fix build errors, and make
 * the unit tests pass.
 *
 * Usually source files are used to write an implementation of
 * global and member functions.
 *
 */

#include "coder.h"

/*
 * To make all unit tests pass you may try to puzzle out the ::encode() algorithm
 * and it should help you to write the decoding one. But there are other ways to
 * make them pass.
 *
 * Good luck!
 *
 */
void Coder::Convert(unsigned int val)
{ 
	unsigned int mask = 1 << (sizeof(int) * 8 - 1);
	for(int i = 0; i < sizeof(int) * 8; i++) 
	{
		if( (val & mask) == 0 ) std::cout << '0' ;
		else std::cout << '1' ; mask >>= 1;
	} std::cout << std::endl ; 
}

void Coder::set(const char* buffer, int size_of_buf)
{
		if(buffer == 0 && size_of_buf != 0){
			throw std::logic_error("buf empty, but size != 0");
		}
		if(size_of_buf == std::numeric_limits<unsigned int>::max()){
			throw std::logic_error("wrong type");
		}
		if(strlen(buffer) > 0 && size_of_buf == 0 ){
			throw std::logic_error("size == 0, but buffer is not empty");
		}
		m_buf = (char *)malloc(size_of_buf+1);
		//strcpy(m_buf, buffer);
		
		unsigned int i = 0;
		while(i < size_of_buf)
		{

			m_buf[i] = buffer[i];
			i++;
		}
		m_buf[i+1] = '\0';
		m_size = size_of_buf;
		
}
void Coder::encode()
{
	//encodes every symbol (except last) as xor of binaries of all next symbols
	::encode( m_buf, m_size );
}
void Coder::decode()
{
	std::cout << "size: " << m_size << '\n';
	
	const char* buf = m_buf;
	int size = m_size;
	
/*	for(int i = 0; i < size; i++)
	{
		std::cout << int(buf[i]) << ' ';
	}
	*/
	std::cout << "strlen vs size " << strlen(buf)<< " " << size << '\n';
	

/*	std::cout << '\n'<<"Results before decode: \n";
	for(int i = 0; i <= size; i++)
	{
		Convert(int(buf[i]));
	
	}	
*/
	char newbuf[size];
	
		newbuf[size-1] = ~buf[size-1];
		newbuf[size] = newbuf[size-1] - 1;

		for(int i = size - 1; i >= 0; i--)
		{	
			newbuf[i - 1] = newbuf[i] ^ buf[i-1];
		}
			
		Coder::set(newbuf, m_size);

/*	std::cout << '\n'<<"Results after decode: \n";
	for(int i = 0; i <= size; i++)
	{
		Convert(int(newbuf[i]));
		//std::cout << int(newbuf[i]) << " ";
	}	
	std::cout << '\n';
*/
}
